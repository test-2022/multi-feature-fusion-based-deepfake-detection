import os
# import cv2
import time
import torch
import torchvision
import numpy as np

from glob import glob
from tqdm import tqdm
import torch.nn.functional as F
from PIL import Image

def get_filter_x(kernel_size):
    if kernel_size == 3:
        filter_x = np.array([[-1 / (2 * np.sqrt(2)), 0, 1 / (2 * np.sqrt(2))],
                             [-1, 0, 1],
                             [-1 / (2 * np.sqrt(2)), 0, 1 / (2 * np.sqrt(2))]], dtype=np.float32)
    elif kernel_size == 5:
        filter_x = np.array([
            [np.cos(6 / 8 * np.pi) / 8, np.cos(5 / 8 * np.pi) / 5, np.cos(4 / 8 * np.pi) / 4,
             np.cos(3 / 8 * np.pi) / 5, np.cos(2 / 8 * np.pi) / 8],
            [np.cos(7 / 8 * np.pi) / 5, -1 / (2 * np.sqrt(2)), 0, 1 / (2 * np.sqrt(2)),
             np.cos(1 / 8 * np.pi) / 5],
            [np.cos(8 / 8 * np.pi) / 4, -1, 0, 1, np.cos(0 / 8 * np.pi) / 4],
            [np.cos(9 / 8 * np.pi) / 5, -1 / (2 * np.sqrt(2)), 0, 1 / (2 * np.sqrt(2)),
             np.cos(15 / 8 * np.pi) / 5],
            [np.cos(10 / 8 * np.pi) / 8, np.cos(11 / 8 * np.pi) / 5, np.cos(12 / 8 * np.pi) / 4,
             np.cos(13 / 8 * np.pi) / 5,
             np.cos(14 / 8 * np.pi) / 8]], dtype=np.float32)
    elif kernel_size == 7:
        filter_x = np.array([[np.cos(9 / 12 * np.pi) / 18, np.cos(8 / 12 * np.pi) / 13, np.cos(7 / 12 * np.pi) / 10,
                              np.cos(6 / 12 * np.pi) / 9,
                              np.cos(5 / 12 * np.pi) / 10, np.cos(4 / 12 * np.pi) / 13,
                              np.cos(3 / 12 * np.pi) / 18],
                             [np.cos(10 / 12 * np.pi) / 13, np.cos(6 / 8 * np.pi) / 8, np.cos(5 / 8 * np.pi) / 5,
                              np.cos(4 / 8 * np.pi) / 4,
                              np.cos(3 / 8 * np.pi) / 5, np.cos(2 / 8 * np.pi) / 8, np.cos(2 / 12 * np.pi) / 13],
                             [np.cos(11 / 12 * np.pi) / 10, np.cos(7 / 8 * np.pi) / 5, -1 / (2 * np.sqrt(2)), 0,
                              1 / (2 * np.sqrt(2)), np.cos(1 / 8 * np.pi) / 5, np.cos(1 / 12 * np.pi) / 10],
                             [np.cos(12 / 12 * np.pi) / 9, np.cos(8 / 8 * np.pi) / 4, -1, 0, 1,
                              np.cos(0 / 8 * np.pi) / 4, np.cos(0 / 12 * np.pi) / 9],
                             [np.cos(13 / 12 * np.pi) / 10, np.cos(9 / 8 * np.pi) / 5, -1 / (2 * np.sqrt(2)), 0,
                              1 / (2 * np.sqrt(2)), np.cos(15 / 8 * np.pi) / 5, np.cos(23 / 12 * np.pi) / 10],
                             [np.cos(14 / 12 * np.pi) / 13, np.cos(10 / 8 * np.pi) / 8, np.cos(11 / 8 * np.pi) / 5,
                              np.cos(12 / 8 * np.pi) / 4,
                              np.cos(13 / 8 * np.pi) / 5, np.cos(14 / 8 * np.pi) / 8, np.cos(22 / 12 * np.pi) / 13],
                             [np.cos(15 / 12 * np.pi) / 18, np.cos(16 / 12 * np.pi) / 13,
                              np.cos(17 / 12 * np.pi) / 10, np.cos(18 / 12 * np.pi) / 9,
                              np.cos(19 / 12 * np.pi) / 10, np.cos(20 / 12 * np.pi) / 13,
                              np.cos(21 / 12 * np.pi) / 18]], dtype=np.float32)
    else:
        raise ValueError("error kernel size %d" % kernel_size)

    return filter_x

def get_filter_y(kernel_size):
    if kernel_size == 3:
        filter_y = np.array([[1 / (2 * np.sqrt(2)), 1, 1 / (2 * np.sqrt(2))],
                             [0, 0, 0],
                             [-1 / (2 * np.sqrt(2)), -1, -1 / (2 * np.sqrt(2))]], dtype=np.float32)

    elif kernel_size == 5:
        filter_y = np.array([
            [np.sin(6 / 8 * np.pi) / 8, np.sin(5 / 8 * np.pi) / 5, np.sin(4 / 8 * np.pi) / 4,
             np.sin(3 / 8 * np.pi) / 5, np.sin(2 / 8 * np.pi) / 8],
            [np.sin(7 / 8 * np.pi) / 5, 1 / (2 * np.sqrt(2)), 1, 1 / (2 * np.sqrt(2)), np.sin(1 / 8 * np.pi) / 5],
            [np.sin(8 / 8 * np.pi) / 4, 0, 0, 0, np.sin(0 / 8 * np.pi) / 4],
            [np.sin(9 / 8 * np.pi) / 5, -1 / (2 * np.sqrt(2)), -1, -1 / (2 * np.sqrt(2)), np.sin(15 / 8 * np.pi) / 5],
            [np.sin(10 / 8 * np.pi) / 8, np.sin(11 / 8 * np.pi) / 5, np.sin(12 / 8 * np.pi) / 4,
             np.sin(13 / 8 * np.pi) / 5,
             np.sin(14 / 8 * np.pi) / 8]], dtype=np.float32)
    elif kernel_size == 7:
        filter_y = np.array([[np.sin(9 / 12 * np.pi) / 18, np.sin(8 / 12 * np.pi) / 13, np.sin(7 / 12 * np.pi) / 10,
                              np.sin(6 / 12 * np.pi) / 9,
                              np.sin(5 / 12 * np.pi) / 10, np.sin(4 / 12 * np.pi) / 13, np.sin(3 / 12 * np.pi) / 18],
                             [np.sin(10 / 12 * np.pi) / 13, np.sin(6 / 8 * np.pi) / 8, np.sin(5 / 8 * np.pi) / 5,
                              np.sin(4 / 8 * np.pi) / 4,
                              np.sin(3 / 8 * np.pi) / 5, np.sin(2 / 8 * np.pi) / 8, np.sin(2 / 12 * np.pi) / 13],
                             [np.sin(11 / 12 * np.pi) / 10, np.sin(7 / 8 * np.pi) / 5, 1 / (2 * np.sqrt(2)), 1,
                              1 / (2 * np.sqrt(2)), np.sin(1 / 8 * np.pi) / 5, np.sin(1 / 12 * np.pi) / 10],
                             [np.sin(12 / 12 * np.pi) / 9, np.sin(8 / 8 * np.pi) / 4, 0, 0, 0,
                              np.sin(0 / 8 * np.pi) / 4, np.sin(0 / 12 * np.pi) / 9],
                             [np.sin(13 / 12 * np.pi) / 10, np.sin(9 / 8 * np.pi) / 5, -1 / (2 * np.sqrt(2)), -1,
                              -1 / (2 * np.sqrt(2)), np.sin(15 / 8 * np.pi) / 5, np.sin(23 / 12 * np.pi) / 10],
                             [np.sin(14 / 12 * np.pi) / 13, np.sin(10 / 8 * np.pi) / 8, np.sin(11 / 8 * np.pi) / 5,
                              np.sin(12 / 8 * np.pi) / 4,
                              np.sin(13 / 8 * np.pi) / 5, np.sin(14 / 8 * np.pi) / 8, np.sin(22 / 12 * np.pi) / 13],
                             [np.sin(15 / 12 * np.pi) / 18, np.sin(16 / 12 * np.pi) / 13,
                              np.sin(17 / 12 * np.pi) / 10, np.sin(18 / 12 * np.pi) / 9,
                              np.sin(19 / 12 * np.pi) / 10, np.sin(20 / 12 * np.pi) / 13,
                              np.sin(21 / 12 * np.pi) / 18]], dtype=np.float32)

    else:
        raise ValueError("error kernel size %d" % kernel_size)
    return filter_y

def plgf_conv(image, filter):

    padding = filter.shape[0]//2

    image_input = torch.Tensor(np.expand_dims(image, axis=(0, 1))).float()
    kernel = torch.Tensor(np.expand_dims(filter, axis=(0, 1))).float()

    kernel = torch.nn.Parameter(kernel, requires_grad=False)
    output = F.conv2d(input=image_input, weight=kernel, padding=padding)
    return output

def normalization(data_in, max_output=255, min_output=0):
    max_value = torch.max(data_in)
    min_value = torch.min(data_in)
    out = ((data_in - min_value)/(max_value - min_value)) * (max_output - min_output) + min_output

    return out

def plgf(input, kernel_size=5):
    """
    :param input:  input is a string of image path or np.ndarray of an image
    :param kernel_size: the plgf conv kernel size (3, 5, 7) default is 5
    :return: a tensor of an image with the shape of (H, W, C)
    """
    if isinstance(input, str):
        image_path = input
        try:
            image = np.array(Image.open(image_path))
        except Exception as e:
            print(e)

    elif isinstance(input, np.ndarray):
        image = input

    if image.ndim != 3 or image.shape[-1] != 3:
        raise ValueError("the image.ndim = %d and image_channel = %d, which is not match requirements"%(image.ndim, image.shape[-1]))

    image = np.where(image>2, image, 2)  # The source code here makes the pixel value must be greater than or equal to 2
    image_r, image_g, image_b = image[:,:,0], image[:,:,1], image[:,:,2]
    image_channel_list = [image_r, image_g, image_b]
    plgf_list = []
    for img in image_channel_list:
        filter_x, filter_y = get_filter_x(kernel_size), get_filter_y(kernel_size)

        plgf_x = plgf_conv(img, filter_x)                # shape of plgf_x/y is (1,C,H,W) → (1,1,256,256)
        plgf_y = plgf_conv(img, filter_y)

        plgf_image = np.arctan(np.sqrt((np.divide(plgf_x, img + 0.0001) ** 2) + (np.divide(plgf_y, img + 0.0001) ** 2)))
        plgf_norm = normalization(plgf_image, 255, 1)
        plgf_list.append(plgf_norm)

    plgf = np.squeeze(np.concatenate(plgf_list, axis=1),axis=0)
    plgf = plgf.transpose((1,2,0))  # convert (C, H, W) to (H, W, C)
    return plgf


if __name__ == '__main__':
    suffix = ['.jpg']
    image_root = 'plgf_in'
    output_root = 'plgf_output'

    image_list = []
    time1 = time.time()
    for each_suffix in suffix:
        image_list_temp = glob(os.path.join(image_root, "**/*"+each_suffix),recursive=True)
        image_list.extend(image_list_temp)
    print("Completed glob all corresponding image in image_root files")
    print("It took time with %.3f s"%(time.time()-time1))

    print("----------------------------------------------------------")
    print("Start generating PLGF image:")

    time1=time.time()

    for image in tqdm(image_list):
        root, pure_name = os.path.split(image)

        plgf_image = Image.fromarray(np.array(plgf(image, kernel_size=5), dtype=np.uint8))
        save_path = image.replace(image_root,output_root)
        save_root,_ = os.path.split(save_path)
        if not os.path.exists(save_root):
            os.makedirs(save_root)
        plgf_image.save(save_path)

    print("Completed generate all corresponding image in image_root files")
    print("It took time with %.3f s"%(time.time()-time1))




